const decoders = {
	Clip({ expression, ...coords }, pos) {
		function transform(pos, key) {
			const value = `${pos}.${key}`;

			return coords[key]
				? `max(${value}, 0.0)`
				: value;
		}
		return compile(expression, `vec3(${transform(pos, "x")}, ${transform(pos, "y")}, ${transform(pos, "z")})`);
	},
	Mirror({ expression, ...coords }, pos) {
		function transform(pos, key) {
			const value = `${pos}.${key}`;

			return coords[key]
				? `abs(${value})`
				: value;
		}
		return compile(expression, `vec3(${transform(pos, "x")}, ${transform(pos, "y")}, ${transform(pos, "z")})`);
	},
	SwapAxis({ newAxis, expression }, pos) {
		return compile(expression, `(${pos}).${newAxis}`);
	},
	Rotation({ expression }, pos) {
		return compile(expression, pos);
	},
	Move({ expression, amount }, pos) {
		const newPos = `${pos} - ${compile(amount)}`;
		return compile(expression, newPos);
	},
	Scale({ expression, amount }, pos) {
		const newPos = `${pos} / ${compile(amount)}`;
		return compile(expression, newPos);
	},
	Elongate({ expression, amount }, pos) {
		const newPos = `${pos} - clamp(${pos}, -${compile(amount)}, ${compile(amount)})`;
		return compile(expression, newPos);
	},
	Coordinate({ x, y, z }, _) {
		return `vec3(${x.toFixed(5)}, ${y.toFixed(5)}, ${z.toFixed(5)})`;
	},
	Point({ }, pos) {
		return `length(${pos})`;
	},
	IsoSurface({ expression, distance }, pos) {
		if (!expression || distance === null)
			return null;

		const compiledLocation = compile(expression, pos);

		if (!compiledLocation)
			return null;

		return `${compiledLocation} - ${distance.toFixed(5)}`;
	},
	Cone({ height, radiusBottom, radiusTop }, pos) {
		return `sdCone(${pos}, ${height}, ${radiusBottom}, ${radiusTop})`
	},
	Cylinder({ height, radius }, pos) {
		return `sdCylinder(${pos}, ${radius}, ${height})`
	},
Box({ size }, pos) {
	const sizeCompiled = compile(size);
	return `length(${pos} - clamp(${pos}, -${sizeCompiled}, ${sizeCompiled}))`
},
	Sphere({ radius }, pos) {
		return `length(${pos}) - ${radius.toFixed(5)}`;
	},
SmoothCombine({ a, b, factor }, pos) {
	return `smin(${compile(a, pos)}, ${compile(b, pos)}, ${factor})`;
},
SmoothSubtract({ base, toSubtract, amount }, pos) {
	return `opSmoothSubtraction(${compile(toSubtract, pos)}, ${compile(base, pos)}, ${amount})`;
},
Subtract({ base, toSubtract }, pos) {
	return `max(-(${compile(toSubtract, pos)}), ${compile(base, pos)})`;
},
	Intersection({ a, b }, pos) {
		return `max(${compile(a, pos)}, ${compile(b, pos)})`;
	},
	Combine({ expressions }, pos) {
		return expressions
			.map(exp => compile(exp, pos))
			.reduce(combineExpressions);
	},
}

function combineExpressions(a, b) {
	return `min(${a}, ${b})`
}

function compile(json, pos) {
	const type = json.type;

	if (!type) {
		console.error(`Type not pressent!`);

		return null;
	}

	// console.log(type);

	const decoder = decoders[type];

	if (!decoder) {
		console.error(`No decoder for type ${type}!`);

		return null;
	}


	return decoder(json, pos);
}

const json = require("./Model");

console.log(compile(json, "pos"));
