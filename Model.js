const toDuplicate = {
	type: "IsoSurface",
	distance: 0.01,
	expression: {
		type: "Mirror",
		y: true,
		expression: {
			type: "Translation",
			amount: {
				type: "Coordinate",
				x: 0.0,
				y: 0.4,
				z: 0.3,
			},
			expression: {
				type: "Elongate",
				amount: {
					type: "Coordinate",
					x: 0.0,
					y: 0.1,
					z: 0.0,
				},
				expression: {
					type: "Cylinder",
					height: 0.3,
					radius: 0.07,
				}
			}
		}
	}
}

const cutOut = {
	type: "Combine",
	expressions: [
		toDuplicate,
		{
			type: "SwapAxis",
			newAxis: "yxz",
			expression: toDuplicate,
		}
	]
}

const mainBody = {
	type: "SmoothCombine",
	factor: 0.01,
	a: {
		type: "IsoSurface",
		distance: 0.01,
		expression: {
			type: "Cone",
			height: 0.2,
			radiusBottom: 0.4,
			radiusTop: 0.2,
		}
	},
	b: {
		type: "Translation",
		amount: {
			type: "Coordinate",
			x: 0.0,
			y: 0.0,
			z: -0.1,
		},
		expression: {
			type: "IsoSurface",
			distance: 0.01,
			expression: {
				type: "Cylinder",
				height: 0.05,
				radius: 0.42,
			}
		}
	}
}

// module.exports = {
// 	type: "SmoothSubtract",
// 	amount: 0.005,
// 	base: mainBody,
// 	toSubtract: cutOut,
// }

module.exports = {
	type: "SmoothSubtract",
	amount: 0.05,
	base: {
		type: "IsoSurface",
		distance: 0.1,
		expression: {
			type: "Box",
			size: {
				type: "Coordinate",
				x: 1,
				y: 1,
				z: 1,
			}
		}
	},
	toSubtract: {
		type: "Move",
		expression: {
			type: "Sphere",
			radius: 1,
		},
		amount: {
			type: "Coordinate",
			x: 1,
			y: 1,
			z: 1,
		}
	},
}
