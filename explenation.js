const dispatchers = {
Coordinate({ x, y, z }, _) {
	return `vec3(${x}, ${y}, ${z})`;
},
Sphere({ radius }, pos) {
	return `length(${pos}) - ${radius}`;
},
Move({ expression, amount }, pos) {
	const newPos = `${pos} - ${compile(amount)}`;
	return compile(expression, newPos);
},
	Elongate({ expression, amount }, pos) {
		const newPos = `${pos} - clamp(${pos}, -${compile(amount)}, ${compile(amount)})`;
		return compile(expression, newPos);
	},
	IsoSurface({ expression, distance }, pos) {
		const compiledLocation = compile(expression, pos);

		return `${compiledLocation} - ${distance.toFixed(5)}`;
	},
	Subtract({ base, toSubtract }, pos) {
		return `max(-(${compile(toSubtract, pos)}), ${compile(base, pos)})`;
	},
	Intersection({ a, b }, pos) {
		return `max(${compile(a, pos)}, ${compile(b, pos)})`;
	},
	Combine({ expressions }, pos) {
		return expressions
			.map(exp => compile(exp, pos))
			.reduce(combineExpressions);
	},
	Scale({ expression, amount }, pos) {
		const newPos = `${pos} / ${compile(amount)}`;
		return compile(expression, newPos);
	},
	SwapAxis({ newAxis, expression }, pos) {
		return compile(expression, `(${pos}).${newAxis}`);
	},
	Rotation({ expression }, pos) {
		return compile(expression, pos);
	},
	Mirror({ expression, ...coords }, pos) {
		function transform(pos, key) {
			const value = `${pos}.${key}`;

			return coords[key]
				? `abs(${value})`
				: value;
		}
		return compile(expression, `vec3(${transform(pos, "x")}, ${transform(pos, "y")}, ${transform(pos, "z")})`);
	},
SmoothSubtract({ base, toSubtract, amount }, pos) {
	return `smax(-(${compile(toSubtract, pos)}), ${compile(base, pos)}, ${amount})`;
},
}

function combineExpressions(a, b) {
	return `min(${a}, ${b})`
}

function compile(expression, pos) {

dispatchers[expression.type](expression);
	
	const type = expression.type;

	if (!type) {
		console.error(`Type not pressent!`);

		return null;
	}

	const decoder = dispatchers[type];

	if (!decoder) {
		console.error(`No decoder for type ${type}!`);

		return null;
	}

	return decoder(expression, pos);
}


